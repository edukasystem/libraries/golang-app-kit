package request

import (
	"bytes"
	"net/http"
)

// RemoteClient :nodoc:
type RemoteClient struct {
	Client *http.Client
	Host   string
}

// Get request
func (c *RemoteClient) Get(endpoint string) (*http.Response, error) {
	request, err := http.NewRequest("GET", c.Host+endpoint, nil)
	if err != nil {
		return nil, err
	}

	return c.Client.Do(request)
}

// Post request
func (c *RemoteClient) Post(endpoint string, body []byte) (*http.Response, error) {
	request, err := http.NewRequest("POST", c.Host+endpoint, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")

	return c.Client.Do(request)
}

// Delete request
func (c *RemoteClient) Delete(endpoint string) (*http.Response, error) {
	request, err := http.NewRequest("DELETE", c.Host+endpoint, nil)
	if err != nil {
		return nil, err
	}

	return c.Client.Do(request)
}
