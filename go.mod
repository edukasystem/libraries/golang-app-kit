module gitlab.com/edukasystem/libraries/golang-app-kit

go 1.12

require (
	github.com/Workiva/go-datastructures v1.0.52
	github.com/go-sql-driver/mysql v1.5.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	go.mongodb.org/mongo-driver v1.3.3
)
