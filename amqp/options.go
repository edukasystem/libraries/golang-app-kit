package amqp

// ExchangeOptions type
type ExchangeOptions struct {
	Name        string
	Type        string
	Durable     bool
	AutoDeleted bool
}

// QueueOptions type
type QueueOptions struct {
	Name        string
	Durable     bool
	AutoDeleted bool
	Exclusive   bool
	MessageTTL  int
}

// ConnectionOptions type
type ConnectionOptions struct {
	Username string
	Password string
	Host     string
	Port     string
}

// Options type
type Options struct {
	Connection ConnectionOptions
	Exchange   ExchangeOptions
	Queue      QueueOptions
	RoutingKey string
}
