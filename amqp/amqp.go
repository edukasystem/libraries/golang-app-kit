package amqp

import amqpClient "github.com/streadway/amqp"

type amqp struct {
	options    Options
	connection *amqpClient.Connection
	channel    *amqpClient.Channel
}

func (m *amqp) setConnection() {
	conn, err := createConnection(m.options.Connection)
	failOnError(err, "Failed to connect to RabbitMQ")
	m.connection = conn
}

func (m *amqp) setChannel() {
	ch, err := m.connection.Channel()
	failOnError(err, "Failed to open a channel")
	m.channel = ch
}

func (m *amqp) declareExchange() {
	args := make(amqpClient.Table)

	args["alternate-exchange"] = AlternateExchangeName

	err := m.channel.ExchangeDeclare(
		m.options.Exchange.Name,
		m.options.Exchange.Type,
		m.options.Exchange.Durable,
		m.options.Exchange.AutoDeleted,
		false,
		false,
		args,
	)

	failOnError(err, "Failed to declare an exchange")
}

func createConnection(options ConnectionOptions) (*amqpClient.Connection, error) {
	if options.Username == "" {
		options.Username = "guest"
	}

	if options.Password == "" {
		options.Password = "guest"
	}

	if options.Host == "" {
		options.Host = "localhost"
	}

	if options.Port == "" {
		options.Port = "5672"
	}

	uri := "amqp://" +
		options.Username +
		":" +
		options.Password +
		"@" +
		options.Host +
		":" +
		options.Port +
		"/"

	return amqpClient.Dial(uri)
}
