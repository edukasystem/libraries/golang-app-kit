package amqp

import (
	"log"

	amqpClient "github.com/streadway/amqp"
)

// Consumer type
type Consumer struct {
	amqp
}

// CreateConsumer create amqp client for receiving messages
func CreateConsumer(options Options) *Consumer {
	consumer := &Consumer{
		amqp: amqp{
			options: options,
		},
	}

	consumer.setConnection()
	consumer.setChannel()
	consumer.declareExchange()
	consumer.declareQueue()
	consumer.bindQueue()

	return consumer
}

// Consume method for consuming messages
func (m *Consumer) Consume(fn func([]byte)) {
	defer m.channel.Close()
	defer m.connection.Close()

	msgs, err := m.channel.Consume(
		m.options.Queue.Name,
		"",
		true,
		m.options.Queue.Exclusive,
		false,
		false,
		nil,
	)

	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			fn(d.Body)
		}
	}()

	log.Printf(" [*] Waiting for messages from \"%s\" queue.", m.options.Queue.Name)
	<-forever
}

func (m *Consumer) declareQueue() {
	args := make(amqpClient.Table)

	if m.options.Queue.MessageTTL > 0 {
		args["x-message-ttl"] = m.options.Queue.MessageTTL
	}

	_, err := m.channel.QueueDeclare(
		m.options.Queue.Name,
		m.options.Queue.Durable,
		m.options.Queue.AutoDeleted,
		m.options.Queue.Exclusive,
		false,
		args,
	)

	failOnError(err, "Failed to declare a queue")
}

func (m *Consumer) bindQueue() {
	err := m.channel.QueueBind(
		m.options.Queue.Name,
		m.options.RoutingKey,
		m.options.Exchange.Name,
		false,
		nil,
	)

	failOnError(err, "Failed to bind a queue")
}
