package tguard

import "sync"

// TGuard struct
type TGuard struct {
	c chan bool
	w sync.WaitGroup
}

// New TGuard
func New(batchSize int) *TGuard {
	return &TGuard{
		c: make(chan bool, batchSize),
	}
}

// Add method
func (m *TGuard) Add() {
	m.c <- true
	m.w.Add(1)
}

// Done method
func (m *TGuard) Done() {
	<-m.c
	m.w.Done()
}

// Wait method
func (m *TGuard) Wait() {
	m.w.Wait()
}
